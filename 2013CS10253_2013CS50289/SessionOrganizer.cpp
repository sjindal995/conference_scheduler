/* 
 * File:   SessionOrganizer.cpp
 * Author: Kapil Thakkar
 * 
 */

#include "SessionOrganizer.h"
#include "Util.h"

SessionOrganizer::SessionOrganizer ( )
{
    parallelTracks = 0;
    papersInSession = 0;
    sessionsInTrack = 0;
    processingTimeInMinutes = 0;
    tradeoffCoefficient = 1.0;
}

SessionOrganizer::SessionOrganizer ( string filename )
{
    start_time = clock();
    readInInputFile ( filename );
    conference = new Conference ( parallelTracks, sessionsInTrack, papersInSession );
    max_organisation = new int**[parallelTracks];
    for(int i=0;i<parallelTracks;i++){
        max_organisation[i] = new int*[sessionsInTrack];
        for(int j=0;j<sessionsInTrack;j++){
            max_organisation[i][j]= new int[papersInSession];
        }
    }
}

void SessionOrganizer::organizePapers ( )
{
    int paperCounter = 0;
    for ( int i = 0; i < conference->getSessionsInTrack ( ); i++ )
    {
        for ( int j = 0; j < conference->getParallelTracks ( ); j++ )
        {
            for ( int k = 0; k < conference->getPapersInSession ( ); k++ )
            {
                conference->setPaper ( j, i, k, paperCounter );
                paperCounter++;
            }
        }
    }
    randomRestart();
    storeMax();
   simulatedAnnealing();
    // hillClimbing();
}

void SessionOrganizer::storeMax(){
    for(int i=0;i<parallelTracks;i++){
        Track tmp_track = conference->getTrack(i);
        for(int j=0;j<sessionsInTrack;j++){
            Session tmp_session = tmp_track.getSession(j);
            for (int k=0;k<papersInSession;k++){
                int paper = tmp_session.getPaper(k);
                max_organisation[i][j][k]=paper;
            }
        }
    }
}

void SessionOrganizer::printMaxOrganisation(){
    for ( int i = 0; i < sessionsInTrack; i++ )
    {
        for ( int j = 0; j < parallelTracks; j++ )
        {
            for ( int k = 0; k < papersInSession; k++ )
            {
                cout<< max_organisation[j][i][k] << " ";
            }
            if ( j != parallelTracks - 1 )
            {
                cout<< "| ";
            }
        }
        cout<< "\r";
    }
}

void SessionOrganizer::randomRestart()
{
    int total_papers=(parallelTracks)*(sessionsInTrack)*(papersInSession);
    int i=0;
    int j=0;
    int k=0;
    int i2=0;
    int j2=0;
    int k2=0;
    int count=0;
    int paper_i_j_k;
    int paper_i2_j2_k2;
    // cout << "initial : " << scoreOrganization() << endl;
    while(count<(total_papers)){
        i = rand()%(sessionsInTrack);
        j = rand()%(parallelTracks);
        k = rand()%(papersInSession);
        i2 = rand()%(sessionsInTrack);
        j2 = rand()%(parallelTracks);
        k2 = rand()%(papersInSession);
        paper_i_j_k = (conference->getTrack(j)).getSession ( i ).getPaper ( k ) ;
        paper_i2_j2_k2 = (conference->getTrack(j2)).getSession ( i2 ).getPaper ( k2 ) ;
        conference->setPaper ( j, i, k, paper_i2_j2_k2 );
        conference->setPaper ( j2, i2, k2, paper_i_j_k );
        count++;
        // cout << "restart score : " << scoreOrganization() << endl;
    }
}

double SessionOrganizer::scorePaper(int paper_index, int session_index, int track_index){
    double score1=0;
    
    Track base_track = conference->getTrack ( track_index );
    Session base_session = base_track.getSession ( session_index );
    int base_paper = base_session.getPaper(paper_index);
    for ( int k = 0; k < base_session.getNumberOfPapers ( ); k++ )
    {
        int index1 = base_session.getPaper ( k );
        if(index1!=base_paper)
            score1 += 1 - distanceMatrix[index1][base_paper];
    }

    double score2 = 0.0;
    for ( int i = 0; i < conference->getParallelTracks ( ); i++ )
    {
        if(i!=track_index){
            Track tmpTrack1 = conference->getTrack ( i );
            Session tmpSession1 = tmpTrack1.getSession ( session_index );
            for ( int k = 0; k < tmpSession1.getNumberOfPapers ( ); k++ )
            {
                int index2 = tmpSession1.getPaper ( k );
                score2 += distanceMatrix[base_paper][index2];
            }
        }
    }
    double score = score1 + tradeoffCoefficient*score2;
    return score;
}


double SessionOrganizer::randomAnnealingMove(double temp,int turn, double score){
    int i = rand()%(conference->getSessionsInTrack());
    int j = rand()%(conference->getParallelTracks());
    int k = rand()%(conference->getPapersInSession());
    int i2=i;
    int j2=j;
    int k2=k;
    // cout << "turn : " << turn <<endl;
    // cout << "ijk : " << i << " " << j << " " << k << endl;
    if(turn==0){
        i2=rand()%(conference->getSessionsInTrack());
        j2=rand()%(conference->getParallelTracks());
    }
    else if(turn==1){
        j2 = rand()%(conference->getParallelTracks());
        k2 = rand()%(conference->getPapersInSession());
    }
    else{
        i2=rand()%(conference->getSessionsInTrack());
        k2=rand()%(conference->getPapersInSession());
    }
    // cout << "i2j2k2 : " << i2 << " " << j2 << " " << k2 << endl;
    double cur_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
    int paper_i_j_k = (conference->getTrack(j)).getSession ( i ).getPaper ( k ) ;
    int paper_i2_j2_k2 = (conference->getTrack(j2)).getSession ( i2 ).getPaper ( k2 ) ;
    conference->setPaper ( j, i, k, paper_i2_j2_k2 );
    conference->setPaper ( j2, i2, k2, paper_i_j_k );
    double new_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
    double delta_e =new_score-cur_score;
    double a=0;
    double b=0;
    if(delta_e<0){
         a= (rand()%1000000)/1000000.0;
         b= exp(delta_e/temp);
    }
    if(delta_e >= 0 ){
        return (score+delta_e);
    }
    else if(a < b) {
        return (score+delta_e);
    }
    else {
        conference->setPaper ( j, i, k, paper_i_j_k );
        conference->setPaper ( j2, i2, k2, paper_i2_j2_k2 );
    }
    return score;
}

void SessionOrganizer::simulatedAnnealing ()
{
    // ofstream out("log.txt");
    double temp=0;
    int turn=0;
    // randomRestart();
    // printSessionOrganiser();
    // cout << "score: " << scoreOrganization()<<endl;
    // exit(0);
    double cur_score=scoreOrganization();
    double best_score=cur_score;
    double prev_score=cur_score;
    int count=0;
    int total_papers=(parallelTracks*sessionsInTrack*papersInSession);
    double cons=0;
    while(float(clock()-start_time)/CLOCKS_PER_SEC < (processingTimeInMinutes*60)-0.1){
        // cout << "curtime1 : " << float(clock()-start_time)/CLOCKS_PER_SEC << endl;
        temp = (processingTimeInMinutes*60)*exp(-(float(clock()-start_time)/CLOCKS_PER_SEC) + cons);
        turn = rand()%3;
        if(count>(total_papers*(total_papers-1))) {
            // cout << "---------------------------------------restart----------------------------\n";
            cons = float(clock()-start_time)/CLOCKS_PER_SEC;
            randomRestart();
            cur_score=scoreOrganization();
            // out << "---------------------------------------restart----------------------------  " <<cur_score<<endl;
            // exit(0);
            count=0;
        }
        prev_score=cur_score;
        cur_score = randomAnnealingMove(temp,turn,cur_score);
        if(cur_score==prev_score){
            count++;
        }
        else
            count=0;
        if(cur_score>best_score){
            best_score=cur_score;
            storeMax();
        }
            // cout << "cur_score : " << cur_score << ", best_score : " << best_score << ", temp : " << temp << endl;
            // out << "cur_score : " << cur_score << ", best_score : " << best_score << ", temp : " << temp << endl;
        // cout << "curtime2 : " << float(clock()-start_time)/CLOCKS_PER_SEC << endl;
    }
    // cout<<"organisation:"<<endl;
    printMaxOrganisation();
    // cout << "score : " << best_score<<endl;
}


double SessionOrganizer::hillClimbingMove(double score)
{
    double cur_score=0, new_score=0;
    int i, j, k, i2, j2, k2;
    int paper_i_j_k, paper_i2_j2_k2;

    long long int counter=0;
    long long int matrix_length = conference->getSessionsInTrack()* conference->getParallelTracks()* conference->getPapersInSession();
    long long int threshold = matrix_length*(matrix_length-1);
    while(float(clock()-start_time)/CLOCKS_PER_SEC < (processingTimeInMinutes*60)-0.1)
    {
        if(counter > threshold)
        {
            randomRestart();
            score = scoreOrganization();
            counter=0;
            // cout << "-------------------------------restart------------------------"<<endl;
        }

        i = rand()%(conference->getSessionsInTrack());
        j = rand()%(conference->getParallelTracks());
        k = rand()%(conference->getPapersInSession());

        i2 = rand()%(conference->getSessionsInTrack());
        j2 = rand()%(conference->getParallelTracks());
        k2 = rand()%(conference->getPapersInSession());

        if(i==i2 and j==j2)
        {   
            continue;
        }
        else
        {
            cur_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
            paper_i_j_k = (conference->getTrack(j)).getSession (i).getPaper (k);
            paper_i2_j2_k2 = (conference->getTrack(j2)).getSession (i2).getPaper (k2);
            conference->setPaper ( j, i, k, paper_i2_j2_k2 );
            conference->setPaper ( j2, i2, k2, paper_i_j_k );
            new_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);

            if(new_score > cur_score)
            {
                return (score+new_score-cur_score);
                // cout<<"break\t"<<new_score<<"\t"<<cur_score<<endl;
            }
            else
            {
                conference->setPaper(j, i, k, paper_i_j_k);
                conference->setPaper(j2, i2, k2, paper_i2_j2_k2);
            }
        }
        counter++;
    }   
    return score;

}

void SessionOrganizer::hillClimbingMove_max()           //not much good... Avoid using.. No strict care of time taken
{
    int sessions_Track = conference->getSessionsInTrack();
    int parallel_Tracks =  conference->getParallelTracks();
    int papers_Session = conference->getPapersInSession();

    float cur_score;
    float new_score;
    float max_delta=0;
    int paper_i_j_k, paper_i2_j2_k2;
    for(int i=0;i<sessions_Track;i++)
    {
        for(int j=0;j<parallel_Tracks;j++)
        {
            for(int i2=0;i2<sessions_Track;i2++)
            {
                for(int j2=0;j2<parallel_Tracks;j2++)
                {
                    if(i==i2 and j==j2)
                        continue;
                    else
                    {
                        for(int k=0;k<papers_Session;k++)
                        {
                            for(int k2=0;k2<papers_Session;k2++)
                            {
                                cur_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
                                // cout<<"hi----------------------"<<endl;
                                paper_i_j_k = (conference->getTrack(j)).getSession (i).getPaper (k);
                                paper_i2_j2_k2 = (conference->getTrack(j2)).getSession (i2).getPaper (k2);
                                conference->setPaper ( j, i, k, paper_i2_j2_k2 );
                                conference->setPaper ( j2, i2, k2, paper_i_j_k );
                                new_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);

                                if(new_score - cur_score > max_delta)
                                {
                                    max_delta = new_score - cur_score;
                                    storeMax();
                                }
                                    
                                conference->setPaper(j, i, k, paper_i_j_k);      
                                conference->setPaper(j2, i2, k2, paper_i2_j2_k2);
                            }
                        }
                    }
                }
            }
        }
    }

    if(max_delta > 0)
    {
        for(int i=0;i<sessions_Track;i++)
        {
            for(int j=0;j<parallel_Tracks;j++)
            {
                for(int k=0;k<papers_Session;k++)
                {
                    conference->setPaper(j, i, k, max_organisation[j][i][k]);
                }
            }
        }
    }
    else
    {
        randomRestart();
    }
}

void SessionOrganizer::hillClimbingMove_Fixiter()           //not that good
{
    double cur_score = 0, new_score = 0, max_delta = 0;
    int counter = 0;
    int i, j, k, i2, j2, k2, paper_i_j_k, paper_i2_j2_k2;
    
    int sessions_Track = conference->getSessionsInTrack();
    int parallel_Tracks = conference->getParallelTracks();
    int papers_Session = conference->getPapersInSession();
    while(float(clock()-start_time)/CLOCKS_PER_SEC < (processingTimeInMinutes*60)-0.1)
    {
        // cout<<"--------------while--------"<<endl;
        if(counter<50)
        {
            // cout<<"counter:        "<<counter<<endl;
            i = rand()%(sessions_Track);
            j = rand()%(parallel_Tracks);
            k = rand()%(papers_Session);

            i2 = rand()%(sessions_Track);
            j2 = rand()%(parallel_Tracks);
            k2 = rand()%(papers_Session);

            if(i==i2 and j==j2)
            {   
                continue;
            }
            else
            {
                cur_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
                paper_i_j_k = (conference->getTrack(j)).getSession (i).getPaper (k);
                paper_i2_j2_k2 = (conference->getTrack(j2)).getSession (i2).getPaper (k2);
                conference->setPaper ( j, i, k, paper_i2_j2_k2 );
                conference->setPaper ( j2, i2, k2, paper_i_j_k );
                new_score = scorePaper(k,i,j)+scorePaper(k2,i2,j2);
                
                if(new_score - cur_score > max_delta)
                {
                    max_delta =  new_score - cur_score;
                    storeMax();
                }   

                conference->setPaper(j, i, k, paper_i_j_k);
                conference->setPaper(j2, i2, k2, paper_i2_j2_k2);
            }
            counter++;
        }
        else
        {
            if(max_delta>0)
            {
                for(int i=0;i<sessions_Track;i++)
                {
                    for(int j=0;j<parallel_Tracks;j++)
                    {
                        for(int k=0;k<papers_Session;k++)
                        {
                            conference->setPaper(j, i, k, max_organisation[j][i][k]);
                        }
                    }
                }

            }
            else
            {
                randomRestart();
            }
            break;
        }
    }
}


void SessionOrganizer::hillClimbing()
{
    double cur_score = scoreOrganization();
    double best_score = cur_score;
    randomRestart();
    while(float(clock()-start_time)/CLOCKS_PER_SEC < (processingTimeInMinutes*60)-0.1)
    {
        cur_score=hillClimbingMove(cur_score);
        if(best_score < cur_score)
        {
            storeMax();
            best_score = cur_score;
        }
        // cout<<"current score:  "<<cur_score<<"\tbest score:  "<<best_score<<endl;
    }
    // cout<<"organisation:"<<endl;
    printMaxOrganisation();
    // cout << "score : " << best_score<<endl;
}


void SessionOrganizer::readInInputFile ( string filename )
{
    vector<string> lines;
    string line;
    ifstream myfile ( filename.c_str () );
    if ( myfile.is_open ( ) )
    {
        while ( getline ( myfile, line ) )
        {
            //cout<<"Line read:"<<line<<endl;
            lines.push_back ( line );
        }
        myfile.close ( );
    }
    else
    {
        cout << "Unable to open input file";
        exit ( 0 );
    }

    if ( 6 > lines.size ( ) )
    {
        cout << "Not enough information given, check format of input file";
        exit ( 0 );
    }

    processingTimeInMinutes = atof ( lines[0].c_str () );
    papersInSession = atoi ( lines[1].c_str () );
    parallelTracks = atoi ( lines[2].c_str () );
    sessionsInTrack = atoi ( lines[3].c_str () );
    tradeoffCoefficient = atof ( lines[4].c_str () );

    int n = lines.size ( ) - 5;
    double ** tempDistanceMatrix = new double*[n];
    for ( int i = 0; i < n; ++i )
    {
        tempDistanceMatrix[i] = new double[n];
    }


    for ( int i = 0; i < n; i++ )
    {
        string tempLine = lines[ i + 5 ];
        string elements[n];
        splitString ( tempLine, " ", elements, n );

        for ( int j = 0; j < n; j++ )
        {
            tempDistanceMatrix[i][j] = atof ( elements[j].c_str () );
        }
    }
    distanceMatrix = tempDistanceMatrix;

    int numberOfPapers = n;
    int slots = parallelTracks * papersInSession*sessionsInTrack;
    if ( slots != numberOfPapers )
    {
        cout << "More papers than slots available! slots:" << slots << " num papers:" << numberOfPapers << endl;
        exit ( 0 );
    }
}

double** SessionOrganizer::getDistanceMatrix ( )
{
    return distanceMatrix;
}

void SessionOrganizer::printSessionOrganiser ( )
{
    conference->printConference ( );
}

double SessionOrganizer::scoreOrganization ( )
{
    // Sum of pairwise similarities per session.
    double score1 = 0.0;
    for ( int i = 0; i < conference->getParallelTracks ( ); i++ )
    {
        Track tmpTrack = conference->getTrack ( i );
        for ( int j = 0; j < tmpTrack.getNumberOfSessions ( ); j++ )
        {
            Session tmpSession = tmpTrack.getSession ( j );
            for ( int k = 0; k < tmpSession.getNumberOfPapers ( ); k++ )
            {
                int index1 = tmpSession.getPaper ( k );
                for ( int l = k + 1; l < tmpSession.getNumberOfPapers ( ); l++ )
                {
                    int index2 = tmpSession.getPaper ( l );
                    score1 += 1 - distanceMatrix[index1][index2];
                }
            }
        }
    }

    // Sum of distances for competing papers.
    double score2 = 0.0;
    for ( int i = 0; i < conference->getParallelTracks ( ); i++ )
    {
        Track tmpTrack1 = conference->getTrack ( i );
        for ( int j = 0; j < tmpTrack1.getNumberOfSessions ( ); j++ )
        {
            Session tmpSession1 = tmpTrack1.getSession ( j );
            for ( int k = 0; k < tmpSession1.getNumberOfPapers ( ); k++ )
            {
                int index1 = tmpSession1.getPaper ( k );

                // Get competing papers.
                for ( int l = i + 1; l < conference->getParallelTracks ( ); l++ )
                {
                    Track tmpTrack2 = conference->getTrack ( l );
                    Session tmpSession2 = tmpTrack2.getSession ( j );
                    for ( int m = 0; m < tmpSession2.getNumberOfPapers ( ); m++ )
                    {
                        int index2 = tmpSession2.getPaper ( m );
                        score2 += distanceMatrix[index1][index2];
                    }
                }
            }
        }
    }
    double score = score1 + tradeoffCoefficient*score2;
    return score;
}
