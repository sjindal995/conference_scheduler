A conference has n papers accepted. Our job is to organize them in a best possible schedule. The schedule has p parallel sessions at a given time. Each session has k papers. And there are a total of t time slots. We can assume that n = t.p.k. 

We first define the characteristics of a good schedule. For any good schedule most people should feel no conflict about which session to attend. That is, (1) all papers in one session should be related to a single theme. And (2) all papers in parallel sessions should be as far away as possible to avoid conflict.

To operationalize this intuition let us assume we are given a function representing the **distance between two papers**: **d(p1,p2)**, such that d is between 0 and 1. We can similarly define a **similarity between two papers** **s(p1,p2) = 1-d(p1,p2)**.

Now we can define the **goodness** of a schedule as follows

**Sum(similarities of all pairs of papers in a session) + C.Sum(distances of all pairs of papers in parallel sessions).**

The constant C trades off the importance of semantic coherence of one session versus reducing
conflict across parallel sessions.

Our goal is to find a schedule with the maximum goodness.